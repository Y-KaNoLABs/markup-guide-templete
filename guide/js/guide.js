/**
 * WEB Markup Guide Templete Resources
 * @license MIT License
 * @version 1.0.0
 * @copyright Y-KaNo LABs | https://bitbucket.org/Y-KaNoLABs/markup-guide-templete/
 * @author Shawna Choi
 * @update 2018-08-08
 */
 'use strict';

$(function(){
	//header
	var innerHd ='';
	innerHd+='<h1>WEB Markup Guide</h1>';
    innerHd+='<a class="btn-nav-view" href="#nav">M</a>';
    innerHd+='<ul id="nav" class="hd-nav">';
    innerHd+='    <li><a href="guide_style.html">style</a></li>';
    innerHd+='    <li><a href="guide_list.html">list</a></li>';
    innerHd+='    <li><a href="guide_components.html">components</a></li>';
   	innerHd+=' </ul>';
   	$('#guide-header').html(innerHd);

   	var innerWrap ='<p style="text-align:center; color:#888;margin-top:12px;margin-bottom:24px;">※ 본 문서는 크롬, 파이어폭스, ie9이상, 사파리등 표준 브라우저에 최적화되어있습니다.</p>';
   $('#guide-wrap').prepend(innerWrap);

	$('#nav li').each(function(){
		var sub = $(this);
		var link = sub.find('a').attr('href');
		var url = location.href;
		var n = url.search(link);
		if(n >= 0){
			sub.addClass('on');
		}
	});

	//list ui
	var scT = $(window).scrollTop();
	var tgList = $('div.guide-h1');
	if(scT > 70){
		tgList.addClass('fix-top');
	}else{
		tgList.removeClass('fix-top');
	}
	$(window).on('scroll',function(){
		scT = $(window).scrollTop();
		if(scT > 70){
			tgList.addClass('fix-top');
		}else{
			tgList.removeClass('fix-top');
		}
		$('#page-list').removeClass('on');
	});

	dressupcodes();
	$('.codes').each(function(i, block) {
	  hljs.highlightBlock(block);
	});

	$('#guide-header .btn-nav-view').on('click',function(e){
		e.preventDefault();
		if($('#nav').hasClass('on')){
			$('#nav').removeClass('on');
		}else{
			$('#nav').addClass('on');
		}
	});

	$('.btn-view-list').on('click',function(e){
		e.preventDefault();
		$(this).toggleClass('on');
		$('#page-list').toggleClass('on');
	});

	$('#guide-wrap').on('click',function(){
		if($('#nav').hasClass('on')){
			$('#nav').removeClass('on');
		}
	});	


	//목차 생성
	//var secArr=[];
	var innerHtml ='';
	$('.guide-sec').each(function(i){
		var innerId='sec'+i;
		var innerTit=$(this).find('.guide-h2').text();
		if($(this).find('.guide-h2').length>0){
			$(this).attr('id',innerId);
			$('#page-list').append('<li><a href="#'+innerId+'">'+innerTit+'</a></li>');
		}
	});

	$('#page-list li a').on('click',function(e){
		e.preventDefault();

		var offsetT;
		if ($('.guide-h1').hasClass('fix-top'))  offsetT=$($(this).attr('href')).offset().top-$('div.guide-h1 h1').outerHeight(true)-20;
		else offsetT=$($(this).attr('href')).offset().top-$('div.guide-h1 h1').outerHeight(true)-290;
		$('body,html').animate({'scrollTop':offsetT});
	});


});


function dressupcodes(){
	var ol, codes, inhtml, mclass;
	var findscript=false, findcss=false;
	var tab='&nbsp;&nbsp;&nbsp;&nbsp;';

	var pres=document.getElementsByTagName('code');

	for(var i=-1, max=pres.length; ++i<max;){
		inhtml=[];

		ol=document.createElement('ol');
		ol.className='codes html';

		codes=pres[i].innerHTML.split(/<br ?\/?>/i);
		if(2>codes.length){
			codes=pres[i].innerHTML.replace(/\r/g, '').split(/\n/i);
		}
		mclass=(10>codes.length)? 'm1' : (100>codes.length)? 'm2' : (1000>codes.length)? 'm3' : 'm4';

		for(var j=-1, jmax=codes.length; ++j<jmax;){

			if(!findscript && codes[j].match(/&lt;script/)){
				findscript=true;
			}else if(!findcss && codes[j].match(/&lt;style/)){
				findcss=true;
			}

			inhtml.push(((j%2)==0)? '<li class="'+mclass+'">' : '<li class="'+mclass+' dbg">');
			inhtml.push((findscript)? '<span class="script">' : (findcss)? '<span class="css">' : '<span>');
			inhtml.push((!codes[j])? '&nbsp;' : codes[j].replace(/\t/g, tab));
			inhtml.push('<\/span><\/li>');

			if(findscript && codes[j].match(/&lt;\/script/)){
				findscript=false;
			}else if(findcss && codes[j].match(/&lt;\/style/)){
				findcss=false;
			}

		}
		ol.innerHTML=inhtml.join('');

		pres[i].style.display='none';
		pres[i].parentNode.insertBefore(ol, pres[i]);

	}
}